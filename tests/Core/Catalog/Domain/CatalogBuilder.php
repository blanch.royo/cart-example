<?php

namespace App\Tests\Core\Catalog\Domain;

use App\Core\Catalog\Domain\Catalog;

class CatalogBuilder
{
    const DEFAULT_ID = '9c334e8d-ad84-4a67-92e8-e4cb2cbe9248';
    const DEFAULT_NAME = 'Informática';

    private string $id;
    private string $name;
    private array $products;

    public function __construct()
    {
        $this->id = self::DEFAULT_ID;
        $this->name = self::DEFAULT_NAME;
        $this->products = [];
    }

    public function build()
    {
        return new Catalog (
            $this->id,
            $this->name,
            $this->products
        );
    }

    public function buildWithProducts(array $products)
    {
        return new Catalog (
            $this->id,
            $this->name,
            $products
        );
    }
}