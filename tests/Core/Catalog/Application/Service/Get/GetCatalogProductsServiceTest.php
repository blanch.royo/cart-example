<?php

namespace App\Tests\Core\Catalog\Application\Service\Get;

use App\Core\Catalog\Application\Service\Get\GetCatalogProductsService;
use App\Core\Catalog\Domain\CatalogRepository;
use App\Tests\Core\Catalog\Domain\CatalogBuilder;
use App\Tests\Core\Product\Domain\ProductBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class GetCatalogProductsServiceTest extends TestCase
{
    const CATALOG_ID = '9c334e8d-ad84-4a67-92e8-e4cb2cbe9248';
    const CATALOG_NAME = 'Informática';
    const CATALOG_SLUG = 'informatica-hoy';

    const PRODUCT_ID = '9c334e8d-ad84-4a67-92e8-e4cb2cbe9233';
    const PRODUCT_NAME = 'Teclado';
    const PRODUCT_PRICE = 10.2;
    const PRODUCT_DISCOUNT_PRICE = 10.2;
    const PRODUCT_STOCK = 10;

    /** @var CatalogRepository | ObjectProphecy  */
    private $catalogRepository;

    private GetCatalogProductsService $getCatalogProductsService;

    protected function setUp(): void
    {
        $this->catalogRepository = $this->prophesize(CatalogRepository::class);
        $this->getCatalogProductsService = new GetCatalogProductsService(
            $this->catalogRepository->reveal()
        );
    }

    protected function tearDown(): void
    {
        unset (
            $this->catalogRepository,
            $this->getCatalogProductsService
        );
    }

    /** @test */
    public function Should_ReturnAllCatalogProducts_When_CatalogHaveAtLeastOne()
    {
        $product = (new ProductBuilder())->build();
        $catalog = (new CatalogBuilder())->buildWithProducts([$product]);
        $this->catalogRepository->find(self::CATALOG_ID)->shouldBeCalled()->willReturn($catalog);
        $return = $this->getCatalogProductsService->__invoke(self::CATALOG_ID);

        $this->assertNotEmpty($return->products());
    }

    /** @test */
    public function Should_ReturnAnEmptyArray_When_CatalogExistsButDoesNotHaveAnyProduct()
    {
        $catalog = (new CatalogBuilder())->build();
        $this->catalogRepository->find(self::CATALOG_ID)->shouldBeCalled()->willReturn($catalog);
        $return = $this->getCatalogProductsService->__invoke(self::CATALOG_ID);

        $this->assertEmpty($return->products());
    }

    /** @test */
    public function Should_ReturnAnEmptyArray_When_CatalogNotExists()
    {
        $this->catalogRepository->find(self::CATALOG_ID)->shouldBeCalled()->willReturn(false);
        $return = $this->getCatalogProductsService->__invoke(self::CATALOG_ID);

        $this->assertEmpty($return->products());
    }

}