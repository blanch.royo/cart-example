<?php

namespace App\Tests\Core\Catalog\Application\Service\Create;

use App\Core\Catalog\Application\Service\Create\CreateCatalogService;
use App\Core\Catalog\Domain\CatalogRepository;
use App\Tests\Core\Catalog\Domain\CatalogBuilder;
use Prophecy\Prophecy\ObjectProphecy;
use PHPUnit\Framework\TestCase;

class CreateCatalogServiceTest extends TestCase
{
    const CATALOG_ID = '9c334e8d-ad84-4a67-92e8-e4cb2cbe9248';
    const CATALOG_NAME = 'Informática';
    const CATALOG_SLUG = 'informatica-hoy';

    /** @var CatalogRepository | ObjectProphecy */
    private $catalogRepository;

    private CreateCatalogService $createCatalogService;

    protected function setUp(): void
    {
        $this->catalogRepository = $this->prophesize(CatalogRepository::class);
        $this->createCatalogService = new CreateCatalogService(
            $this->catalogRepository->reveal()
        );
    }

    protected function tearDown(): void
    {
        unset (
            $this->catalogRepository,
            $this->createCatalogService
        );
    }

    /** @test */
    public function Should_CreateACatalog_When_AllParametersAreOkay()
    {
        $catalog = (new CatalogBuilder())->build();
        $this->catalogRepository->save($catalog)->shouldBeCalled();
        $this->createCatalogService->__invoke(self::CATALOG_ID, self::CATALOG_NAME);
    }
}