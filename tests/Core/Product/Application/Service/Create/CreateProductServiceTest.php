<?php

namespace App\Tests\Core\Product\Application\Service\Create;

use App\Core\Product\Application\Service\Create\CreateProductService;
use App\Core\Product\Domain\ProductRepository;
use App\Tests\Core\Product\Domain\ProductBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class CreateProductServiceTest extends TestCase
{
    const PRODUCT_ID = '9c334e8d-ad84-4a67-92e8-e4cb2cbe9233';
    const PRODUCT_NAME = 'Teclado';
    const PRODUCT_PRICE = 10.2;
    const PRODUCT_DISCOUNT_PRICE = 10.2;
    const PRODUCT_STOCK = 10;

    /** @var ProductRepository | ObjectProphecy */
    private $productRepository;

    private CreateProductService $createProductService;

    protected function setUp(): void
    {
        $this->productRepository = $this->prophesize(ProductRepository::class);
        $this->createProductService = new CreateProductService(
            $this->productRepository->reveal()
        );
    }

    protected function tearDown(): void
    {
        unset (
            $this->productRepository,
            $this->createProductService
        );
    }

    /** @test */
    public function Should_CreateACatalog_When_AllParametersAreOkay()
    {
        $product = (new ProductBuilder())->build();
        $this->productRepository->save($product)->shouldBeCalled();
        $this->createProductService->__invoke(self::PRODUCT_ID, self::PRODUCT_NAME, self::PRODUCT_PRICE, self::PRODUCT_DISCOUNT_PRICE, self::PRODUCT_STOCK);
    }
}