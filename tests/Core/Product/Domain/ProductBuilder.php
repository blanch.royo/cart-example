<?php

namespace App\Tests\Core\Product\Domain;

use App\Core\Product\Domain\Product;

class ProductBuilder
{
    const PRODUCT_ID = '9c334e8d-ad84-4a67-92e8-e4cb2cbe9233';
    const PRODUCT_NAME = 'Teclado';
    const PRODUCT_PRICE = 10.2;
    const PRODUCT_DISCOUNT_PRICE = 10.2;
    const PRODUCT_STOCK = 10;

    private string $id;
    private string $name;
    private float $price;
    private float $discountPrice;
    private int $stock;
    private array $catalogs;

    public function __construct()
    {
        $this->id = self::PRODUCT_ID;
        $this->name = self::PRODUCT_NAME;
        $this->price = self::PRODUCT_PRICE;
        $this->discountPrice = self::PRODUCT_DISCOUNT_PRICE;
        $this->stock = self::PRODUCT_STOCK;
        $this->catalogs = [];
    }

    public function build()
    {
        return new Product (
            $this->id,
            $this->name,
            $this->price,
            $this->discountPrice,
            $this->stock
        );
    }
}