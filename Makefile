.PHONY: build start stop down test shell configure

.PHONY: build
build:
	docker-compose build

.PHONY: start
start:
	docker-compose up -d

.PHONY : configure
configure:
	@-docker-compose exec php composer install
	@-docker-compose exec php apps/core/bin/console doctrine:database:create --if-not-exists
	@-docker-compose exec php apps/core/bin/console doctrine:migrations:migrate --no-interaction

.PHONY: stop
stop:
	docker-compose stop

.PHONY: down
down:
	docker-compose down

.PHONY: test
test:
	docker-compose exec php vendor/bin/phpunit -c phpunit.xml.dist

.PHONY: shell
shell:
	docker-compose exec php /bin/sh