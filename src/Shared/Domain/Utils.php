<?php

namespace App\Shared\Domain;

use DateTimeInterface;
use ReflectionClass;

class Utils
{
    public static function dateToString(DateTimeInterface $date): string
    {
        return $date->format(DateTimeInterface::ATOM);
    }

    public static function extractClassName(object $object): string
    {
        $reflect = new ReflectionClass($object);
        return $reflect->getShortName();
    }
}