<?php

namespace App\Shared\Infrastructure\Bus\Query;

use App\Shared\Domain\Bus\Query\Query;
use Exception;

final class QueryHandlerNotFoundException extends Exception
{
    public function __construct(Query $query)
    {
        $queryClass = get_class($query);
        parent::__construct("The query <$queryClass> hasn't a query handler associated");
    }
}