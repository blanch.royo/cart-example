<?php

namespace App\Shared\Infrastructure\Persistence\Doctrine;

use App\Shared\Domain\Aggregate\AggregateRoot;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

abstract class DoctrineRepository
{
    private EntityManagerInterface $entityManager;
    private ObjectRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository($this->entityName());
        $this->entityManager = $entityManager;
    }

    public function save(AggregateRoot $entity, bool $commit = true)
    {
        $this->em()->persist($entity);
        if ($commit) {
            $this->em()->flush();
        }
    }

    protected function em(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    protected function repository(): ObjectRepository
    {
        return $this->repository;
    }

    public function findOneBy(array $criteria)
    {
        return $this->repository()->findOneBy($criteria);
    }

    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function find($id)
    {
        return $this->repository()->find($id);
    }

    public function removeBy($criteria, bool $commit = true)
    {
        $entitiesToRemove = $this->createQueryBuilder('entities')->addCriteria($criteria)->getQuery()->getResult();
        foreach ($entitiesToRemove as $entityToRemove) {
            $this->em()->remove($entityToRemove);
        }

        if ($commit) {
            $this->em()->flush();
        }
    }

    protected function createQueryBuilder(string $alias = null, string $indexBy = null)
    {
        return $this
            ->em()
            ->createQueryBuilder()
            ->select($alias)
            ->from($this->entityName(), $alias, $indexBy);
    }

    abstract protected function entityName(): string;
}