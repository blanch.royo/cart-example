<?php

namespace App\Core\OrderLine\Domain;

use App\Core\Order\Domain\Order;
use App\Core\Product\Domain\Product;
use App\Shared\Domain\Aggregate\AggregateRoot;

final class OrderLine extends AggregateRoot
{
    private string $id;
    private Order $order;
    private int $quantity;
    private float $unitPrice;
    private float $totalPrice;
    private int $type;
    private Product $product;

    public function __construct(string $id, Order $order, int $quantity, float $unitPrice, float $totalPrice, int $type, Product $product)
    {
        $this->id = $id;
        $this->order = $order;
        $this->quantity = $quantity;
        $this->unitPrice = $unitPrice;
        $this->totalPrice = $totalPrice;
        $this->type = $type;
        $this->product = $product;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getUnitPrice(): float
    {
        return $this->unitPrice;
    }

    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }
}