<?php

namespace App\Core\Order\Domain;

use App\Shared\Domain\Aggregate\AggregateRoot;
use Doctrine\Common\Collections\ArrayCollection;
use DateTime;

final class Order extends AggregateRoot
{
    private string $id;
    private int $locator;
    private int $status;
    private DateTime $createdAt;
    private int $totalQuantity;
    private float $totalAmount;
    private DateTime $updatedAt;
    private ArrayCollection $orderLines;

    public function __construct(string $id, int $locator, int $status, DateTime $createdAt, Datetime $updatedAt, int $totalQuantity, float $totalAmount)
    {
        $this->id = $id;
        $this->locator = $locator;
        $this->status = $status;
        $this->createdAt = $createdAt;
        $this->totalQuantity = $totalQuantity;
        $this->totalAmount = $totalAmount;
        $this->updatedAt = $updatedAt;
        $this->orderLines = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getLocator(): int
    {
        return $this->locator;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getTotalQuantity(): int
    {
        return $this->totalQuantity;
    }

    public function getTotalAmount(): float
    {
        return $this->totalAmount;
    }

    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    public function getOrderLines(): ArrayCollection
    {
        return $this->orderLines;
    }
}