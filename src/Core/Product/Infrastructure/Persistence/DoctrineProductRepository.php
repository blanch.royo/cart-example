<?php

namespace App\Core\Product\Infrastructure\Persistence;

use App\Core\Product\Domain\Product;
use App\Core\Product\Domain\ProductRepository;
use App\Shared\Domain\Aggregate\AggregateRoot;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class DoctrineProductRepository extends DoctrineRepository implements ProductRepository
{
    public function save(AggregateRoot $product, bool $commit = true): void
    {
        parent::save($product);
    }

    protected function entityName(): string
    {
        return Product::class;
    }
}