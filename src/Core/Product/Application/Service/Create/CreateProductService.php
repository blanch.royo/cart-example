<?php

namespace App\Core\Product\Application\Service\Create;

use App\Core\Product\Domain\Product;
use App\Core\Product\Domain\ProductRepository;

class CreateProductService
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function __invoke(string $id, string $name, float $price, float $discountPrice, int $stock)
    {
        $product = Product::create($id, $name, $price, $discountPrice, $stock);
        $this->productRepository->save($product);
    }
}