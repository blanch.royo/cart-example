<?php

namespace App\Core\Product\Application\Service\Create;

use App\Shared\Domain\Bus\Command\Command;

class CreateProductCommand implements Command
{
    private string $name;
    private float $price;
    private float $discountPrice;
    private int $stock;

    public function __construct(string $name, float $price, float $discountPrice, int $stock)
    {
        $this->name = $name;
        $this->price = $price;
        $this->discountPrice = $discountPrice;
        $this->stock = $stock;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getDiscountPrice(): float
    {
        return $this->discountPrice;
    }

    public function getStock(): int
    {
        return $this->stock;
    }
}