<?php

namespace App\Core\Product\Application\Service\Create;

use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\ValueObject\Uuid;

class CreateProductCommandHandler implements CommandHandler
{
    private CreateProductService $createProductService;

    public function __construct(CreateProductService $createProductService)
    {
        $this->createProductService = $createProductService;
    }

    public function __invoke(CreateProductCommand $command): void
    {
        $id = Uuid::random()->value();
        $name = $command->getName();
        $price = $command->getPrice();
        $discountPrice = $command->getDiscountPrice();
        $stock = $command->getStock();

        $this->createProductService->__invoke($id, $name, $price, $discountPrice, $stock);
    }
}