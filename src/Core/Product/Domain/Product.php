<?php

namespace App\Core\Product\Domain;

use App\Shared\Domain\Aggregate\AggregateRoot;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Product extends AggregateRoot
{
    const DEFAULT_SOLD = 0;

    private string $id;
    private string $name;
    private float $price;
    private float $discountPrice;
    private int $stock;
    private int $sold;
    private Collection $catalogs;

    public function __construct(string $id, string $name, float $price, float $discountPrice, int $stock)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->discountPrice = $discountPrice;
        $this->stock = $stock;
        $this->sold = self::DEFAULT_SOLD;
        $this->catalogs = new ArrayCollection();
    }

    public static function create(string $id, string $name, float $price, float $discountPrice, int $stock): self
    {
        return new self(
            $id,
            $name,
            $price,
            $discountPrice,
            $stock
        );
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getDiscountPrice(): float
    {
        return $this->discountPrice;
    }

    public function getStock(): int
    {
        return $this->stock;
    }

    public function getSold(): int
    {
        return $this->sold;
    }

    public function getCatalogs(): ArrayCollection
    {
        return $this->catalogs;
    }
}