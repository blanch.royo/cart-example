<?php

namespace App\Core\Product\Domain;

interface ProductRepository
{
    public function save(Product $product): void;
}