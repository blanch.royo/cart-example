<?php

namespace App\Core\Catalog\Infrastructure\Persistence;

use App\Core\Catalog\Domain\Catalog;
use App\Core\Catalog\Domain\CatalogRepository;
use App\Shared\Domain\Aggregate\AggregateRoot;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class DoctrineCatalogRepository extends DoctrineRepository implements CatalogRepository
{
    public function save(AggregateRoot $catalog, bool $commit = true): void
    {
        parent::save($catalog);
    }

    protected function entityName(): string
    {
        return Catalog::class;
    }
}