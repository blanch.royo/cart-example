<?php

namespace App\Core\Catalog\Domain;

interface CatalogRepository
{
    public function save(Catalog $catalog): void;
    public function find($id);
}