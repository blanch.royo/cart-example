<?php

namespace App\Core\Catalog\Domain;

use App\Shared\Domain\Aggregate\AggregateRoot;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Catalog extends AggregateRoot
{
    private string $id;
    private string $name;
    private string $slug;
    private Collection $products;

    public function __construct(string $id, string $name, array $products = [])
    {
        $this->id = $id;
        $this->name = $name;
        $this->slug = $this->slugifyName($name);
        $this->products = new ArrayCollection($products);
    }

    public static function create(string $id, string $name): self
    {
        return new self($id, $name);
    }

    public static function createWithProducts(string $id, string $name, array $products): self
    {
        return new self($id, $name, $products);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    private function slugifyName(string $name)
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $name)));
    }
}