<?php

namespace App\Core\Catalog\Application\Service\Create;

use App\Core\Catalog\Domain\Catalog;
use App\Core\Catalog\Domain\CatalogRepository;

class CreateCatalogService
{
    private CatalogRepository $catalogRepository;

    public function __construct(CatalogRepository $catalogRepository)
    {
        $this->catalogRepository = $catalogRepository;
    }

    public function __invoke(string $id, string $name)
    {
        $catalog = Catalog::create($id, $name);
        $this->catalogRepository->save($catalog);
    }
}