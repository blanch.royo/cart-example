<?php

namespace App\Core\Catalog\Application\Service\Create;

use App\Shared\Domain\Bus\Command\Command;

class CreateCatalogCommand implements Command
{
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}