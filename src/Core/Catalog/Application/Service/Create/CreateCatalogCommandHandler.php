<?php

namespace App\Core\Catalog\Application\Service\Create;

use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\ValueObject\Uuid;

class CreateCatalogCommandHandler implements CommandHandler
{
    private CreateCatalogService $createCatalogService;

    public function __construct(CreateCatalogService $createCatalogService)
    {
        $this->createCatalogService = $createCatalogService;
    }

    public function __invoke(CreateCatalogCommand $command): void
    {
        $id = Uuid::random()->value();
        $name = $command->getName();

        $this->createCatalogService->__invoke($id, $name);
    }
}