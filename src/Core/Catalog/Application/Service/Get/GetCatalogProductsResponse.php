<?php

namespace App\Core\Catalog\Application\Service\Get;

use App\Shared\Domain\Bus\Query\Response;

class GetCatalogProductsResponse implements Response
{
    private array $products;

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function products(): array
    {
        return $this->products;
    }
}