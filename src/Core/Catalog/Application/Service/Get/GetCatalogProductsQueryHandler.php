<?php

namespace App\Core\Catalog\Application\Service\Get;

use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\ValueObject\Uuid;

class GetCatalogProductsQueryHandler implements QueryHandler
{
    private GetCatalogProductsService $service;

    public function __construct(GetCatalogProductsService $service)
    {
        $this->service = $service;
    }

    public function __invoke(GetCatalogProductsQuery $query): GetCatalogProductsResponse
    {
        return $this->service->__invoke($query->getCatalogId());
    }
}