<?php

namespace App\Core\Catalog\Application\Service\Get;

use App\Core\Catalog\Domain\Catalog;
use App\Core\Catalog\Domain\CatalogRepository;

final class GetCatalogProductsService
{
    private CatalogRepository $catalogRepository;

    public function __construct(CatalogRepository $catalogRepository)
    {
        $this->catalogRepository = $catalogRepository;
    }

    public function __invoke(string $catalogId): GetCatalogProductsResponse
    {
        /** @var Catalog $catalog */
        $catalog = $this->catalogRepository->find($catalogId);

        if (!$catalog) {
            return new GetCatalogProductsResponse([]);
        }

        return new GetCatalogProductsResponse($catalog->getProducts()->toArray());
    }
}