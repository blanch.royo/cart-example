<?php

namespace App\Core\Catalog\Application\Service\Get;

use App\Shared\Domain\Bus\Query\Query;

class GetCatalogProductsQuery implements Query
{
    private string $catalogId;

    public function __construct(string $catalogId)
    {
        $this->catalogId = $catalogId;
    }

    public function getCatalogId(): string
    {
        return $this->catalogId;
    }
}