# Cart example
This is an example of a PHP application using Domain-Driven Design and Command Query Responsibility Segregation (CQRS).

## About this project
This project is for test use. In this case, I used this project as an API to improve my skills and apply all my knowledge.

## Domain
The project uses mainly Symfony implementations, but as it is decoupled, I could use any other framework.

Actually, is divided in two bounded contexts:
- **Core**: here is the main backend application.
- **Shared**: here it goes everything you need between bounded contexts, for example `utils` or the `command/query bus` definition and implementation.

Inside the `Core` bounded context, we can find:
- **Catalog**: all the catalogs and products.
- **Cart**: this includes all of a cart relationship, starting from the Order.
- **Shared**: here it goes all the shared code between the different modules.

In the future, maybe it's necessary adding a new Bounded context, such as a `backoffice`, and move the `product/catalog` creation there, and leaving only the reading endpoints in `core`.

## Hexagonal Architecture
This project also applies the Hexagonal Architecture in order to accomplish the Domain-Driven Design. Because of this, inside our `modules` we can find the different layers represented by folders like that:
- **Application**: where our use cases live, the entry point to our system.
- **Domain**: where our business logic and domain model resides.
- **Infrastructure**: where our adapters get implemented.

## CQRS
Also, a CQRS implementation is needed because of how well it fits in a DDD application, and in terms of scalability, is a better way to separate the Read/Write actions.

- **Query Bus**: returns a result without changing the state of the system (Free of side effects).
- **Command Bus**: changes the state of a system, but with no response (With Side effects).

The Command/Query bus used in this project was implemented with Symfony Messenger in a SYNC way.
- [Command Bus](src/Shared/Infrastructure/Bus/Command/InMemorySymfonyCommandBus.php)
- [Query Bus](src/Shared/Infrastructure/Bus/Query/InMemorySymfonyQueryBus.php)

## How to start
Requirements:
- :whale: **Docker** :whale:

### Steps:
- Clone this project

```
git clone https://gitlab.com/blanch.royo/cart-example.git
```
- Build containers

```
make build
```
- Start containers

```
make start
```
- Install composer dependencies and create database if not exists, also execute migrations

```
make configure
```
- Health check endpoint

```
http://localhost:8000/core/health-check
```

### Run tests
```
make test
```

