<?php

namespace Apps\Core\Controller\Product;

use App\Core\Product\Application\Service\Create\CreateProductCommand;
use App\Shared\Infrastructure\Symfony\ApiController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class CreateProductController extends ApiController
{
    public function __invoke(Request $request): Response
    {
        $name = $request->request->get('name');
        $price = $request->request->get('price');
        $discountPrice = $request->request->get('discountPrice');
        $stock = $request->request->get('stock');

        $this->dispatch(new CreateProductCommand($name, $price, $discountPrice, $stock));
        return new Response('', Response::HTTP_CREATED);
    }
}