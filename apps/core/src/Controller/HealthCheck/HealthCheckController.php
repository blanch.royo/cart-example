<?php

namespace Apps\Core\Controller\HealthCheck;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HealthCheckController
{
    public function __invoke(Request $request): JsonResponse
    {
        return new JsonResponse(
            ['core' => "OK"],
            Response::HTTP_OK
        );
    }
}