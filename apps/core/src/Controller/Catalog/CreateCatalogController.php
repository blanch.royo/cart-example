<?php

namespace Apps\Core\Controller\Catalog;

use App\Core\Catalog\Application\Service\Create\CreateCatalogCommand;
use App\Shared\Infrastructure\Symfony\ApiController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateCatalogController extends ApiController
{
    public function __invoke(Request $request): Response
    {
        $name = $request->request->get('name');
        $this->dispatch(new CreateCatalogCommand($name));
        return new Response('', Response::HTTP_CREATED);
    }
}