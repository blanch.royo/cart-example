<?php

namespace Apps\Core\Controller\Catalog;

use App\Core\Catalog\Application\Service\Get\GetCatalogProductsQuery;
use App\Shared\Infrastructure\Symfony\ApiController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ListCatalogProductsController extends ApiController
{
    public function __invoke(string $id, Request $request): Response
    {
        $catalogId = $id;
        $response = $this->ask(new GetCatalogProductsQuery($catalogId));
        return new Response($response, Response::HTTP_OK);
    }
}